Mr Label Maker
==============

Script for automatic labeling of issues and merge requests for projects
hosted on gitlab.freedesktop.org.

Dependencies:

- python3
- pip3

Run instructions:

.. code-block:: sh

  $ pip3 install . --user
  $ ~/.local/bin/mr_label_maker.py -p mesa -m -i -t "$TOKEN" -d
